data "ibm_is_zones" "regional" {
  region = var.region
}

data "ibm_is_vpc" "lab" {
  name = var.vpc_name
}

data "ibm_is_security_groups" "vpc" {
  vpc_name = var.vpc_name
}

data "ibm_is_ssh_key" "regional" {
  name = var.existing_ssh_key
}