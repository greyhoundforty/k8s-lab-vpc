variable "ibmcloud_api_key" {
  description = "IBM Cloud API key to use for deploying cluster resources"
  type        = string
}

variable "vpc_name" {
  description = "Name of the VPC to use for the cluster"
  type        = string
}

variable "region" {
  description = "VPC Region where the cluster will be deployed."
  type        = string
}

variable "owner" {
  description = "Owner tag that will be applied to all cluster resources"
  type        = string
  default     = "ryantiffany"
}

variable "existing_resource_group" {
  description = "Existing resource group to use for cluster resources. If none is provided, a new one will be created."
  type        = string
  default     = ""
}

variable "existing_ssh_key" {
  description = "Existing ssh key that will be attached to all compute instances."
  type        = string
}

variable "controller_instance_count" {
  description = "Number of controllers nodes to deploy for the cluster."
  type        = number
  default     = 3
}

variable "worker_instance_count" {
  description = "Number of worker nodes to deploy for the cluster."
  type        = number
  default     = 2
}

variable "logging_ingestion_key" {
  description = "LogDNA ingestion key to use for cluster logging"
  type        = string
}

variable "monitoring_api_key" {
  description = "Sysdig API key to use for cluster monitoring"
  type        = string
}
