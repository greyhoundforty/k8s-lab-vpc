## Notes and to do tracking, mainly for me:
## 
# cluster security group: data.ibm_is_security_groups.vpc[0].id
# dmz security group: data.ibm_is_security_groups.vpc[1].id
#

# Generate a random string for the prefix
resource "random_string" "project_prefix" {
  length  = 4
  special = false
  upper   = false
  numeric = false
}

module "resource_group" {
  source                       = "git::https://github.com/terraform-ibm-modules/terraform-ibm-resource-group.git?ref=v1.0.5"
  resource_group_name          = var.existing_resource_group == null ? "${local.basename}-resource-group" : null
  existing_resource_group_name = var.existing_resource_group
}

module "bastion" {
  source            = "./modules/compute"
  instance_name     = "${local.basename}-bastion"
  vpc_id            = data.ibm_is_vpc.lab.id
  zone              = local.vpc_zones[0].zone
  subnet_id         = data.ibm_is_vpc.lab.subnets[0].id
  resource_group_id = module.resource_group.resource_group_id
  security_group_id = data.ibm_is_vpc.lab.security_group[1].group_id
  ssh_key           = data.ibm_is_ssh_key.regional.id
  user_data         = null
  tags              = concat(local.tags, ["zone:${local.vpc_zones[1].zone}", "node-type:bastion"])
}

module "controllers" {
  count             = var.controller_instance_count
  source            = "./modules/compute"
  instance_name     = "${local.basename}-controller-${count.index}"
  vpc_id            = data.ibm_is_vpc.lab.id
  zone              = local.vpc_zones[1].zone
  subnet_id         = data.ibm_is_vpc.lab.subnets[1].id
  resource_group_id = module.resource_group.resource_group_id
  security_group_id = data.ibm_is_vpc.lab.security_group[0].group_id
  ssh_key           = data.ibm_is_ssh_key.regional.id
  user_data         = null
  tags              = concat(local.tags, ["zone:${local.vpc_zones[1].zone}", "node-type:k8s-controller"])
}

resource "ibm_is_floating_ip" "bastion" {
  name           = "${local.basename}-bastion-ip"
  resource_group = module.resource_group.resource_group_id
  target         = module.bastion.compute_instance.primary_network_interface[0].id
  tags           = concat(local.tags, ["zone:${local.vpc_zones[0].zone}"])
}

module "workers" {
  count             = var.worker_instance_count
  source            = "./modules/compute"
  instance_name     = "${local.basename}-worker-${count.index}"
  vpc_id            = data.ibm_is_vpc.lab.id
  zone              = local.vpc_zones[1].zone
  subnet_id         = data.ibm_is_vpc.lab.subnets[1].id
  resource_group_id = module.resource_group.resource_group_id
  security_group_id = data.ibm_is_vpc.lab.security_group[0].group_id
  ssh_key           = data.ibm_is_ssh_key.regional.id
  user_data = templatefile("${path.module}/workers.tftpl", {
    pod_cidr = "172.17.${count.index}.0/24"
  })
  tags = concat(local.tags, ["zone:${local.vpc_zones[1].zone}", "node-type:k8s-worker"])
}

resource "ibm_is_vpc_routing_table_route" "woker_pod_cidr_route" {
  depends_on    = [module.workers]
  count         = var.worker_instance_count
  vpc           = data.ibm_is_vpc.lab.id
  routing_table = data.ibm_is_vpc.lab.default_routing_table
  zone          = local.vpc_zones[0].zone
  name          = "${local.basename}-worker-${count.index}-pod-route"
  destination   = "172.17.${count.index}.0/24"
  action        = "deliver"
  priority      = 2
  next_hop      = module.workers[count.index].compute_instance_ip
}

module "private_dns" {
  source                = "./modules/dns"
  name                  = local.basename
  vpc_crn               = data.ibm_is_vpc.lab.crn
  healthcheck_subnet_id = data.ibm_is_vpc.lab.subnets[1].id
  resource_group_id     = module.resource_group.resource_group_id
  region                = var.region
  tags                  = local.tags
  controller_ips        = module.controllers[*].compute_instance_ip
  worker_ips            = module.workers[*].compute_instance_ip
}

module "certificates" {
  source                      = "./modules/certificates"
  kube_apiserver_loadbalancer = module.private_dns.kube_apiserver_loadbalancer
  controllers                 = module.controllers[*].compute_instance
  workers                     = module.workers[*].compute_instance
  name                        = local.basename
  worker_instance_count       = var.worker_instance_count
}

module "ansible_inventory" {
  source                      = "./modules/ansible"
  project_name                = local.basename
  bastion_public_ip           = ibm_is_floating_ip.bastion.address
  controllers                 = module.controllers[*].compute_instance
  workers                     = module.workers[*].compute_instance
  kube_apiserver_loadbalancer = module.private_dns.kube_apiserver_loadbalancer
  logging_ingestion_key       = var.logging_ingestion_key
  region                      = var.region
  monitoring_api_key          = var.monitoring_api_key
}