#!/bin/bash
set -e -o pipefail

if [[ "$1" == "plan" ]]; then
  terraform init -upgrade=true
  terraform fmt -recursive
  terraform plan -out "$(terraform workspace show).tfplan" -var-file=terraform.tfvars
fi

if [[ "$1" == "destroy" ]]; then
  terraform destroy -auto-approve -var-file=terraform.tfvars
  rm -f modules/certificates/generated/*
fi

if [[ "$1" == "clean" ]]; then
  rm -f modules/certificates/generated/*
  rm -f .terraform
  rm -f *.tfplan
  rm -f *.tfstate*
fi

if [[ "$1" == "apply" ]]; then
  terraform plan -out "$(terraform workspace show).tfplan" -var-file=terraform.tfvars
  terraform apply "$(terraform workspace show).tfplan"
fi
