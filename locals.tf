locals {
  basename = "${random_string.project_prefix.result}-rt"

  zones = length(data.ibm_is_zones.regional.zones)
  vpc_zones = {
    for zone in range(local.zones) : zone => {
      zone = "${var.region}-${zone + 1}"
    }
  }

  tags = [
    "owner:${var.owner}",
    "provider:ibm",
    "region:${var.region}",
    "vpc:${var.vpc_name}",
    "project:${local.basename}",
  ]
}