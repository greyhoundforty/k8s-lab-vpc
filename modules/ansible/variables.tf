variable "bastion_public_ip" {}
variable "controllers" {}
variable "workers" {}
variable "kube_apiserver_loadbalancer" {}
variable "monitoring_api_key" {}
variable "logging_ingestion_key" {}
variable "region" {}
variable "project_name" {}