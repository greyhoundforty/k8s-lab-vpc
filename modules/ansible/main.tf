resource "local_file" "ansible-inventory" {
  content = templatefile("${path.module}/templates/inventory.tftpl",
    {
      bastion_ip  = var.bastion_public_ip
      controllers = var.controllers
      workers     = var.workers
    }
  )
  filename = "${path.module}/inventory.ini"
}

resource "local_file" "ansible-vars" {
  content = templatefile("${path.module}/templates/playbook_vars.tftpl",
    {
      kube_apiserver_loadbalancer = var.kube_apiserver_loadbalancer
      logging_ingestion_key       = var.logging_ingestion_key
      monitoring_api_key          = var.monitoring_api_key
      region                      = var.region
      project_name                = var.project_name
    }
  )
  filename = "${path.module}/playbooks/vars/cluster-vars.yml"
}