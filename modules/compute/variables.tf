variable "instance_name" {}
variable "zone" {}
variable "vpc_id" {}
variable "subnet_id" {}
variable "resource_group_id" {}
variable "security_group_id" {}
variable "tags" {}
variable "ssh_key" {}
variable "user_data" {}

variable "allow_ip_spoofing" {
  description = "Allow IP Spoofing"
  type        = bool
  default     = true
}

variable "instance_profile" {
  description = "The profile to use for the instance"
  type        = string
  default     = "bx2-4x16"
}

variable "instance_image" {
  description = "The name of an existing OS image to use. You can list available images with the command 'ibmcloud is images'."
  type        = string
  default     = "ibm-ubuntu-22-04-2-minimal-amd64-1"
}