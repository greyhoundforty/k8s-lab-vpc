output "compute_instance" {
  value = ibm_is_instance.compute
}

output "compute_instance_ip" {
  value = ibm_is_instance.compute.primary_network_interface[0].primary_ip[0].address
}