resource "ibm_is_instance" "compute" {
  name           = var.instance_name
  vpc            = var.vpc_id
  image          = data.ibm_is_image.base.id
  profile        = var.instance_profile
  resource_group = var.resource_group_id

  metadata_service {
    enabled            = true
    protocol           = "https"
    response_hop_limit = 3
  }

  boot_volume {
    auto_delete_volume = true
  }

  primary_network_interface {
    subnet            = var.subnet_id
    allow_ip_spoofing = var.allow_ip_spoofing
    security_groups   = [var.security_group_id]
  }

  user_data = var.user_data

  zone = var.zone
  keys = [var.ssh_key]
  tags = var.tags
}