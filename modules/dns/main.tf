resource "ibm_resource_instance" "private_dns" {
  name              = "${var.name}-dns-instance"
  resource_group_id = var.resource_group_id
  location          = "global"
  service           = "dns-svcs"
  plan              = "standard-dns"
  tags              = var.tags
}

resource "ibm_dns_zone" "kubernetes" {
  depends_on  = [ibm_resource_instance.private_dns]
  name        = "${var.name}.labdomain"
  instance_id = ibm_resource_instance.private_dns.guid
  description = "Private DNS Zone for VPC K8s cluster."
  label       = "k8sthehardway"
}

resource "ibm_dns_permitted_network" "permitted_network" {
  instance_id = ibm_resource_instance.private_dns.guid
  zone_id     = ibm_dns_zone.kubernetes.zone_id
  vpc_crn     = var.vpc_crn
  type        = "vpc"
}

resource "ibm_dns_resource_record" "controllers" {
  count       = length(var.controller_ips)
  instance_id = ibm_resource_instance.private_dns.guid
  zone_id     = ibm_dns_zone.kubernetes.zone_id
  type        = "A"
  name        = "controller-${count.index}"
  rdata       = var.controller_ips[count.index]
  ttl         = 3600
}

resource "ibm_dns_resource_record" "workers" {
  count       = length(var.worker_ips)
  instance_id = ibm_resource_instance.private_dns.guid
  zone_id     = ibm_dns_zone.kubernetes.zone_id
  type        = "A"
  name        = "worker-${count.index}"
  rdata       = var.worker_ips[count.index]
  ttl         = 3600
}

resource "ibm_dns_glb_monitor" "gslb_healthcheck" {
  depends_on  = [ibm_dns_zone.kubernetes]
  name        = "${var.name}-glb-monitor"
  instance_id = ibm_resource_instance.private_dns.guid
  description = "Private DNS health check for kubes apiservers"
  interval    = 60
  retries     = 3
  timeout     = 5
  port        = 6443
  type        = "TCP"
}

resource "ibm_dns_glb_pool" "k8s_pdns_glb_pool" {
  depends_on                = [ibm_dns_zone.kubernetes]
  name                      = "${var.name}-glb-pool"
  instance_id               = ibm_resource_instance.private_dns.guid
  enabled                   = true
  healthy_origins_threshold = 1
  origins {
    name    = "controller-0"
    address = var.controller_ips[0]
    enabled = true
  }
  origins {
    name    = "controller-1"
    address = var.controller_ips[1]
    enabled = true
  }
  origins {
    name    = "controller-2"
    address = var.controller_ips[2]
    enabled = true
  }

  monitor             = ibm_dns_glb_monitor.gslb_healthcheck.monitor_id
  healthcheck_region  = var.region
  healthcheck_subnets = [var.healthcheck_subnet_id]
}


resource "ibm_dns_glb" "k8s_pdns_glb" {
  depends_on    = [ibm_dns_glb_pool.k8s_pdns_glb_pool]
  name          = "k8s-api.${var.name}.labdomain"
  instance_id   = ibm_resource_instance.private_dns.guid
  zone_id       = ibm_dns_zone.kubernetes.zone_id
  ttl           = 120
  enabled       = true
  fallback_pool = ibm_dns_glb_pool.k8s_pdns_glb_pool.pool_id
  default_pools = [ibm_dns_glb_pool.k8s_pdns_glb_pool.pool_id]
}