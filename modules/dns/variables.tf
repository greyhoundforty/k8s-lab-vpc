variable "name" {}
variable "resource_group_id" {}
variable "vpc_crn" {}
variable "tags" {}
variable "healthcheck_subnet_id" {}
variable "region" {}
variable "controller_ips" {}
variable "worker_ips" {}